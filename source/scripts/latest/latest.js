let i, tabcontent, tablinks, prod__card;
tabcontent = document.getElementsByClassName("prod");
tablinks = document.getElementsByClassName("tablinks");
prod__card = document.getElementsByClassName("prod__card");

$(document).ready(function () {
  document.getElementById("new").style.display = "flex";
  tablinks[0].className += " active";
});

function openTab(evt, tabName) {
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "flex";
  evt.currentTarget.className += " active";
  AOS.refresh();
}
