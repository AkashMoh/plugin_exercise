$(document).ready(function () {
  $(".panel").hide();
  $(".accordion > .acc__head").on("click", function () {
    if ($(this).hasClass("active")) {
      $(this).siblings(".panel").slideUp(300);
      $(this).removeClass("active");
      $(this).children("img").attr("src", "../../images/accordion/plus.svg");
    } else {
      $(this).siblings(".panel").slideDown(300);
      $(this).addClass("active");
      $(this).children("img").attr("src", "../../images/accordion/minus.svg");
    }
  });

  //popup
  $(".popup > a").magnificPopup({
    type: "inline",

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: "auto",

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: "my-mfp-zoom-in",
  });
});
